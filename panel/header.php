<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600|Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="./css/spur.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    <script src="./js/chart-js-config.js"></script>
    <style type="text/css">
        .alert{margin-top: 20px;}
        #baka-success, #baka-failed, #baka-warring{display: none;}
    </style>

    <?php
/**
Copyright @ 2019: Dang Hai
**/
require ($_SERVER['DOCUMENT_ROOT'].'/pro1121/includes/db.php');
include ($_SERVER['DOCUMENT_ROOT'].'/pro1121/includes/session.php');
include ($_SERVER['DOCUMENT_ROOT'].'/pro1121/includes/functions.php');

if (isset($_GET['page'])) {
    $page = $_GET['page'];
    settype($page, 'int');
    if ($page < 1) {
        $page = 1;
    }
    echo '<title>'.$name.' | Trang '.$page.'</title>';
}else{
    $page = 1;
    echo '<title>'.$name.'</title>';
}

?>
</head>
<body>
    <div class="dash">
     <div class="dash-nav dash-nav-dark">
        <header>
            <a href="#!" class="menu-toggle">
                <i class="fas fa-bars"></i>
            </a>
            <a href="index.php" class="spur-logo"><i class="fas fa-bolt"></i> <span>PRO1121</span></a>
        </header>
        <nav class="dash-nav-list">
            <a href="index.php" class="dash-nav-item">
                <i class="fas fa-home"></i> Trang chủ </a>
                <div class="dash-nav-dropdown">
                    <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
                        <i class="fas fa-users"></i> Bình luận </a>
                        <div class="dash-nav-dropdown-menu">
                            <a href="comments.php" class="dash-nav-dropdown-item">Tất cả bình luận</a>
                        </div>
                    </div>
                    <?php if (getTypeUser($login_session) == 9) {
                        echo '
                     <div class="dash-nav-dropdown">
                     <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
                     <i class="fas fa-users"></i> Người dùng </a>
                     <div class="dash-nav-dropdown-menu">
                     <a href="users.php" class="dash-nav-dropdown-item">Danh sách</a>
                     </div>
                     </div>
                     <div class="dash-nav-dropdown ">
                     <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
                     <i class="fas fa-cube"></i> Quản lý </a>
                     <div class="dash-nav-dropdown-menu">
                     <a href="category.php" class="dash-nav-dropdown-item">Chuyên mục</a>
                     <a href="post.php" class="dash-nav-dropdown-item">Truyện</a>

                     <a href="chapter.php" class="dash-nav-dropdown-item">Chapter</a>

                     </div>
                     </div>
                     ';}?>
                     <div class="dash-nav-dropdown">
                        <a href="#!" class="dash-nav-item dash-nav-dropdown-toggle">
                            <i class="fas fa-info"></i> Về chúng tôi </a>
                            <div class="dash-nav-dropdown-menu">
                                <a href="https://fb.com/dangofficial" target="_blank" class="dash-nav-dropdown-item">Dang Hai</a>
                                <a href="#" target="_blank" class="dash-nav-dropdown-item">App Truyện</a>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="dash-app">
                    <header class="dash-toolbar">
                        <a href="#!" class="menu-toggle">
                            <i class="fas fa-bars"></i>
                        </a>
                        <a href="#!" class="searchbox-toggle">
                            <i class="fas fa-search"></i>
                        </a>
                        <form class="searchbox" action="#!">
                            <a href="#!" class="searchbox-toggle"> <i class="fas fa-arrow-left"></i> </a>
                            <button type="submit" class="searchbox-submit"> <i class="fas fa-search"></i> </button>
                            <input type="text" class="searchbox-input" placeholder="type to search">
                        </form>
                        <div class="tools">

                            <a href="#!" class="tools-item">
                                <i class="fas fa-bell"></i>
                                <i class="tools-item-count">4</i>
                            </a>
                            <div class="dropdown tools-item">
                                <a href="#" class="" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-user"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                    <a class="dropdown-item" href="#!"><b><?php echo 'Xin chào, '.$login_session; ?></b></a>
                                    <a class="dropdown-item" href="#!">Lý lịch</a>
                                    <a class="dropdown-item" href="logout.php">Đăng xuất</a>
                                </div>
                            </div>
                        </div>
                    </header>

