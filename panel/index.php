<?php 

$name = "Trang chủ";
include 'header.php';

?>
    
            <main class="dash-content">
                <div class="container-fluid">
                    <div class="row dash-row">
                        <div class="col-xl-4">
                            <div class="stats stats-primary">
                                <h3 class="stats-title"> Thành viên </h3>
                                <div class="stats-content">
                                    <div class="stats-icon">
                                        <i class="fas fa-user"></i>
                                    </div>
                                    <div class="stats-data">
                                        <div class="stats-number"><?php echo countIndex('user'); ?></div>
                                        <div class="stats-change">
                                        
                                            <span class="stats-timeframe">người đã tham gia</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="stats stats-success ">
                                <h3 class="stats-title"> Số truyện </h3>
                                <div class="stats-content">
                                    <div class="stats-icon">
                                        <i class="fas fa-book"></i>
                                    </div>
                                    <div class="stats-data">
                                        <div class="stats-number"><?php echo countIndex('post'); ?></div>
                                        <div class="stats-change">
                                        
                                            <span class="stats-timeframe">trên tổng số</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="stats stats-danger">
                                <h3 class="stats-title"> Số chương truyện </h3>
                                <div class="stats-content">
                                    <div class="stats-icon">
                                        <i class="fas fa-book-reader"></i>
                                    </div>
                                    <div class="stats-data">
                                        <div class="stats-number"><?php echo countIndex('chapter'); ?></div>
                                         <span class="stats-timeframe">trên tổng số</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6">
                           <div class="card spur-card">
                                <div class="card-header bg-warning text-dark">
                                    <div class="spur-card-icon">
                                        <i class="fas fa-chart-bar"></i>
                                    </div>
                                    <div class="spur-card-title"> Lời giới thiệu </div>
                                    <div class="spur-card-menu">
                                        <div class="dropdown">
                                            <a class="spur-card-menu-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(30px, 30px, 0px);">
                                                <a class="dropdown-item" href="#">Ẩn / hiện</a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body text-dark"> 
                                    <p>Chào mừng các bạn đã đến với trang quản trị app quản lý truyện Android version 1.0</p>
                                    <p>1. Hệ thống backend được quản lý trên web bằng mã nguồn mysql, php, html5 và css</p>
                                    <p>2. Mọi tính năng hoạt động nhanh chóng và mượt mà</p>
                                     <p>3. Hệ thống thành viên bao gồm các chức năng bình luận, like truyện</p>
                                     <p>4. Hệ thống truyện được post theo chương, dễ sử dụng</p>
                                    <p>Tác giả: <b>Dang Hai</b></p>
                                    <p>Bản quyền: Version 1.0_2019_Team_6</p>

                                 </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card spur-card">
                                <div class="card-header">
                                    <div class="spur-card-icon">
                                        <i class="fas fa-bell"></i>
                                    </div>
                                    <div class="spur-card-title"> Bình luận mới </div>
                                </div>
                                <div class="card-body ">
                                    <div class="notifications">
                                        <a href="#!" class="notification">
                                            <div class="notification-icon">
                                                <i class="fas fa-inbox"></i>
                                            </div>
                                            <div class="notification-text">New comment</div>
                                            <span class="notification-time">21 days ago</span>
                                        </a>
                                        <a href="#!" class="notification">
                                            <div class="notification-icon">
                                                <i class="fas fa-inbox"></i>
                                            </div>
                                            <div class="notification-text">New comment</div>
                                            <span class="notification-time">21 days ago</span>
                                        </a>
                                        <a href="#!" class="notification">
                                            <div class="notification-icon">
                                                <i class="fas fa-inbox"></i>
                                            </div>
                                            <div class="notification-text">New comment</div>
                                            <span class="notification-time">21 days ago</span>
                                        </a>
                                        <div class="notifications-show-all">
                                            <a href="#!">Show all</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
     <?php 
     include 'footer.php';?>