<?php

$name = "PRO1121 - Quản lý thành viên";
include 'header.php';
if (getTypeUser($login_session) != 9) {
    die( 'Bạn không có đủ quyền vô trang này');
}
?>
            <main class="dash-content">
                <div class="container-fluid">
                    <h1 class="dash-title">Thống kê</h1>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card spur-card">
                                <div class="card-header">
                                    <div class="spur-card-icon">
                                        <i class="fas fa-table"></i>
                                    </div>
                                    <div class="spur-card-title">Người dùng đã đăng ký</div>
                                </div>
                                <div class="card-body ">
                                    <table class="table table-striped table-in-card">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Username</th>
                                                <th scope="col">Email</th>
                                                <th scope="col">Chức vụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php getAllUser($page); ?>
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                               <div class="btn-group ml-2 mb-1" role="group" aria-label="First group">
                                          <?php baka_pagination($page, 'user') ?>
                                        </div>
                        </div>
                        
                    </div>
                </div>
            </main>
      <?php include 'footer.php'?>