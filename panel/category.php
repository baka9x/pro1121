<?php
$name = 'PRO1121 - Quản lý chuyên mục';
include 'header.php';
if (getTypeUser($login_session) != 9) {
    die("Bạn không có đủ quyền vô trang này");
}
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST['title'];
    $slug = rwurl($_POST['title']); 
    $description =$_POST['description']; 
   insertCategory($title, $slug, $description);
}

?>
                                <main class="dash-content">
                                    <div class="container-fluid">
                                        <h1 class="dash-title">Thêm Chuyên Mục</h1>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="card spur-card">
                                                    <div class="card-header">
                                                        <div class="spur-card-icon">
                                                            <i class="fas fa-chart-bar"></i>
                                                        </div>
                                                        <div class="spur-card-title"> Biểu mẫu </div>
                                                    </div>
                                                    <div class="card-body ">
                                                        <form method="post">
                                                            <div class="form-group">
                                                                <label for="title">Tên chuyên mục</label>
                                                                <input type="text" name="title" class="form-control" id="title" placeholder="Điền tên chuyên mục" required>
                                                            </div>

                                                          
                                                            <div class="form-group">
                                                                <label for="description">Mô tả</label>
                                                                <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary">Thêm chuyên mục</button>
                                                        </form>
                                                    </div>


                                                </div>
                                                    <!--thông báo-->
                                                  <div class="alert alert-success" id="baka-success" role="alert"> Tạo thành công! </div>
                                                 <div class="alert alert-danger" id="baka-failed" role="alert"> Tạo không thành công! </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="card spur-card">
                                                    <div class="card-header">
                                                        <div class="spur-card-icon">
                                                            <i class="fas fa-table"></i>
                                                        </div>
                                                        <div class="spur-card-title">Các chuyên mục đã tạo</div>
                                                    </div>
                                                    <div class="card-body ">
                                                        <table class="table table-in-card">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">Tiêu đề</th>
                                                                    <!--<th scope="col">Đường dẫn</th>-->
                                                                    <th scope="col">Mô tả</th>
                                                                    <th scope="col">Thao tác</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                               <?php getAllCategory($page) ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                         <div class="btn-group ml-2 mb-1" role="group" aria-label="First group">
                                          <?php baka_pagination($page, 'category') ?>
                                        </div>
                                            </div>

                                        </div>
                                    </main>
                               <?php include 'footer.php'?>