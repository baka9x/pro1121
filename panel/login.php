<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600|Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="./css/spur.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    <script src="./js/chart-js-config.js"></script>
    <title>PRO1121 - Đăng Nhập</title>
    <style type="text/css">
       .alert{margin-top: 20px;}
         #baka-success, #baka-failed, #baka-warring{display: none;}
    </style>
</head>

 <?php 
require ($_SERVER['DOCUMENT_ROOT'].'/pro1121/includes/db.php');
session_start();

if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 

  $myusername = mysqli_real_escape_string($conn,$_POST['username']);
  $mypassword = mysqli_real_escape_string($conn,md5($_POST['password'])); 

  $sql = "SELECT id FROM user WHERE username = '$myusername' and password = '$mypassword'";
  $result = mysqli_query($conn,$sql);
  $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
  $active = $row['active'];

  $count = mysqli_num_rows($result);

      // If result matched $myusername and $mypassword, table row must be 1 row

  if($count == 1) {

    $_SESSION['login_user'] = $myusername;
  echo '<style>#baka-success{display: block;}</style>';
    header("location: index.php");
  }else {
     echo '<style>#baka-failed{display: block;}</style>';
  }
}
?>

<body>
    <div class="form-screen">
        <a href="index.html" class="spur-logo"><i class="fas fa-bolt"></i> <span>PRO1121</span></a>
        <div class="card account-dialog">
            <div class="card-header bg-primary text-white"> Vui lòng đăng nhập </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" id="username" aria-describedby="usernameHelp" placeholder="Nhập username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Nhập mật khẩu">
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Remember me</label>
                        </div>
                    </div>
                    <div class="account-dialog-actions">
                        <button type="submit" class="btn btn-primary">Đăng nhập</button>
                        <a class="account-dialog-link" href="signup.php">Tạo tài khoản</a>
                    </div>
                </form>
            </div>
        </div>

        <div class="alert alert-success" id="baka-success" role="alert"> Đăng nhập thành công! </div>
        <div class="alert alert-danger" id="baka-failed" role="alert"> Sai tài khoản hoặc mật khẩu! </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="./js/spur.js"></script>
</body>

</html>