<?php
$name = 'PRO1121 - Quản lý Truyện';
include 'header.php';
if (getTypeUser($login_session) != 9) {
    die( 'Bạn không có đủ quyền vô trang này');
}

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST['title'];
    $slug = rwurl($_POST['title']); 
    $thumbnail =$_POST['thumbnail']; 
     $category = $_POST['category'];
    $author = $_POST['author']; 
    $content =$_POST['content']; 
   insertPost($title, $slug, $thumbnail, $content, $category, $author);
}

?>
                                <main class="dash-content">
                                    <div class="container-fluid">
                                        <h1 class="dash-title">Thêm Truyện</h1>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="card spur-card">
                                                    <div class="card-header">
                                                        <div class="spur-card-icon">
                                                            <i class="fas fa-chart-bar"></i>
                                                        </div>
                                                        <div class="spur-card-title"> Biểu mẫu </div>
                                                    </div>
                                                    <div class="card-body ">
                                                        <form method="post">
                                                            <div class="form-group">
                                                                <label for="title">Tên truyện</label>
                                                                <input type="text" name="title" class="form-control" id="title" placeholder="Điền tên truyện" required>
                                                            </div>

                                                            <!--<div class="form-group">
                                                                <label for="slug">Đường dẫn link</label>
                                                                <input type="text" name="slug" class="form-control" id="slug" placeholder="Ví dụ:bai-viet" required>
                                                            </div>-->

                                                             <div class="form-group">
                                                                <label for="thumbnail">Link ảnh thumbnail</label>
                                                                <input type="text" name="thumbnail" class="form-control" id="thumbnail" placeholder="Định dạng: png, jpg, jpeg..." required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="category">Chuyên mục</label>
                                                                <select class="form-control" id="category" name="category">
                                                               <?php getCategoryToPost();?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="title">Tác giả</label>
                                                                <input type="text" name="author" class="form-control" id="author" value="<?php echo $login_session; ?>" placeholder="Điền tên tác giả" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="content">Nội dung</label>
                                                                <textarea class="form-control" name="content" id="content" rows="3"></textarea>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary">Thêm Truyện</button>
                                                        </form>
                                                    </div>


                                                </div>
                                                    <!--thông báo-->
                                                  <div class="alert alert-success" id="baka-success" role="alert"> Tạo thành công! </div>
                                                 <div class="alert alert-danger" id="baka-failed" role="alert"> Tạo không thành công! </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="card spur-card">
                                                    <div class="card-header">
                                                        <div class="spur-card-icon">
                                                            <i class="fas fa-table"></i>
                                                        </div>
                                                        <div class="spur-card-title">Các đầu truyện</div>
                                                    </div>
                                                    <div class="card-body ">
                                                        <table class="table table-in-card">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">Tên truyện</th>
                                                                    <!--<th scope="col">Đường dẫn</th>-->
                                                                    <th scope="col">Ngày tạo</th>
                                                                    <th scope="col">Thumbnail</th>
                                                                    <th scope="col">Thao tác</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                               <?php getAllPost($page) ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                         <div class="btn-group ml-2 mb-1" role="group" aria-label="First group">
                                          <?php baka_pagination($page, 'post') ?>
                                        </div>
                                            </div>

                                        </div>
                                    </main>
                        <?php include 'footer.php'?>