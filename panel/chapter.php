<?php
$name = 'PRO1121 - Quản lý Chương truyện';
include 'header.php';
if (getTypeUser($login_session) != 9) {
    die( 'Bạn không có đủ quyền vô trang này');
}
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST['title'];
    $slug = rwurl($_POST['title']); 
    $content =$_POST['content']; 
    $post = $_POST['idx'];
  insertChapter($title, $slug, $content, $post);
}

?>
                                <main class="dash-content">
                                    <div class="container-fluid">
                                        <h1 class="dash-title">Thêm chương truyện</h1>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="card spur-card">
                                                    <div class="card-header">
                                                        <div class="spur-card-icon">
                                                            <i class="fas fa-chart-bar"></i>
                                                        </div>
                                                        <div class="spur-card-title"> Biểu mẫu </div>
                                                    </div>
                                                    <div class="card-body ">
                                                        <form method="post">
                                                            <div class="form-group">
                                                                <label for="title">Tên chương</label>
                                                                <input type="text" name="title" class="form-control" id="title" placeholder="Ví dụ: Chapter 1" required>
                                                            </div>

                                                               <div class="form-group">
                                                                <label for="slug">Dường dẫn link</label>
                                                                <input type="text" name="slug" class="form-control" id="slug" placeholder="Ví dụ: chapter-1" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="idx">Id truyện gốc:</label>
                                                                <input type="text" name="idx" class="form-control" id="idx" placeholder="Lấy từ id Truyện" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="content">Content</label>
                                                                <textarea class="form-control" name="content" id="content" rows="3"></textarea>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary">Thêm Chương</button>
                                                        </form>
                                                    </div>


                                                </div>
                                                    <!--thông báo-->
                                                  <div class="alert alert-success" id="baka-success" role="alert"> Tạo thành công! </div>
                                                 <div class="alert alert-danger" id="baka-failed" role="alert"> Tạo không thành công! </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="card spur-card">
                                                    <div class="card-header">
                                                        <div class="spur-card-icon">
                                                            <i class="fas fa-table"></i>
                                                        </div>
                                                        <div class="spur-card-title">Tất cả chương truyện</div>
                                                    </div>
                                                    <div class="card-body ">
                                                        <table class="table table-in-card">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">Tiêu đề</th>
                                                                    <!--<th scope="col">Đường dẫn</th>-->
                                                                    <th scope="col">Ngày tạo</th>
                                                                    <th scope="col">Id Truyện Gốc</th>
                                                                    <th scope="col">Thao tác</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                               <?php getAllChapter($page) ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                         <div class="btn-group ml-2 mb-1" role="group" aria-label="First group">
                                          <?php baka_pagination($page, 'chapter') ?>
                                        </div>
                                            </div>

                                        </div>
                                    </main>
                        <?php include 'footer.php'?>