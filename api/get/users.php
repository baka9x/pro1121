<?php

require ($_SERVER['DOCUMENT_ROOT'].'/pro1121/includes/db.php');

$query = "SELECT `id`, `username`, `password`, `email`, `type`, `is_online` FROM `user` WHERE 1";



$data = mysqli_query($conn, $query);

class User{
	function User($id, $username, $password, $email, $type, $is_online){

		$this->id = $id;
		$this->username = $username;
		$this->password = $password;
		$this->email = $email;
		$this->type = $type;
		$this->is_online = $is_online;


	}
}


$arrayUser = array();
while ($row = mysqli_fetch_assoc($data)) {
    array_push($arrayUser, new User($row['id']
										,$row['username']
										,$row['password']
										,$row['email']
										,$row['type']
										,$row['is_online']));

}

echo json_encode($arrayUser);

?>