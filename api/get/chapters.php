<?php

require ($_SERVER['DOCUMENT_ROOT'].'/pro1121/includes/db.php');

$query = "SELECT chapter.id, chapter.chapter_title, chapter.chapter_datetime, chapter.chapter_content, chapter.post ,post.title FROM chapter INNER JOIN post ON chapter.post = post.id";

$data = mysqli_query($conn, $query);

class Post{
	function Post($id, $title, $datetime, $content, $postId, $postTitle){

		$this->id = $id;
		$this->title = $title;
		$this->datetime = $datetime;
		$this->content = $content;
		$this->postId = $postId;
		$this->postTitle = $postTitle;
	
	}
}


$arrayChapter = array();
while ($row = mysqli_fetch_assoc($data)) {
	array_push($arrayChapter, new Post($row['id']
		,$row['chapter_title']
		,$row['chapter_datetime']
		,$row['chapter_content']
		,$row['post']
		,$row['title']));


}

echo json_encode($arrayChapter);

?>