<?php

require ($_SERVER['DOCUMENT_ROOT'].'/pro1121/includes/db.php');

$query = "SELECT post.id, post.title, post.datetime, post.thumbnail, post.content, post.category, post.author, category.category_title FROM post INNER JOIN category ON post.category = category.id";

$data = mysqli_query($conn, $query);

class Post{
	function Post($id, $title, $datetime, $thumbnail, $content, $category, $author, $categoryTitle){

		$this->id = $id;
		$this->title = $title;
		$this->datetime = $datetime;
		$this->thumbnail = $thumbnail;
		$this->content = $content;
		$this->category = $category;
		$this->author = $author;
		$this->categoryTitle = $categoryTitle;
		

	}
}


$arrayPost = array();
while ($row = mysqli_fetch_assoc($data)) {
	array_push($arrayPost, new Post($row['id']
		,$row['title']
		,$row['datetime']
		,$row['thumbnail']
		,$row['content']
		,$row['category']
		,$row['author']
		,$row['category_title']));


}

echo json_encode($arrayPost);

?>