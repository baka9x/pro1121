<?php

require ($_SERVER['DOCUMENT_ROOT'].'/pro1121/includes/db.php');

$query = "SELECT comment.id, comment.cmt_content, comment.cmt_author, comment.cmt_post, comment.cmt_datetime, post.title, user.username FROM comment INNER JOIN post ON comment.cmt_post = post.id INNER JOIN user ON comment.cmt_author = user.id";

$data = mysqli_query($conn, $query);


class Comment{
	function Comment($id, $cmt_content, $cmt_author, $cmt_post, $cmt_datetime, $postTitle, $username){

		$this->id = $id;
		$this->cmt_content = $cmt_content;
		$this->cmt_author = $cmt_author;
		$this->cmt_post = $cmt_post;
		$this->cmt_datetime = $cmt_datetime;
		$this->postTitle = $postTitle;
		$this->username = $username;
	}
}


$arrayComment = array();
while ($row = mysqli_fetch_assoc($data)) {
	array_push($arrayComment, new Post($row['id']
		,$row['cmt_content']
		,$row['cmt_author']
		,$row['cmt_post']
		,$row['cmt_datetime']
		,$row['title']
		,$row['username']));


}

echo json_encode($arrayComment);


?>