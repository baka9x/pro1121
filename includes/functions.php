<?php
/*
/*@ Project's name: HDV Truyện
/*@ Project's description: ALL DAO Function
/*@						   Assignment Pro1121
/*@						   
/*@ Author: Dang Hai - Team 6
/*@ Year: 2019
*/
error_reporting(E_ERROR | E_PARSE);

function getTypeUser($session){
	require 'db.php';
	$query = "SELECT `username`, `type` FROM `user` WHERE `username` LIKE '$session'";
	$data = mysqli_query($conn, $query);
		if (mysqli_num_rows($data) > 0) {
		while ($row = mysqli_fetch_assoc($data)) {
			return $row['type'];

		}
	}
	mysqli_close($conn);
}

function getAllUser($page){
	require 'db.php';
	$limit = 5;
	$query = "SELECT `id` FROM `user`";
	$row = mysqli_query($conn, $query);
	$total_page = mysqli_num_rows($row);
	$current_page = ceil($total_page / $limit);
	if ($page > $current_page) {
		$page = $current_page;
	}
	$from = ($page - 1 ) * $limit;
	$query = "SELECT * FROM user LIMIT $from, $limit";
	$data = mysqli_query($conn, $query);
	if (mysqli_num_rows($data) > 0) {
		while ($row = mysqli_fetch_assoc($data)) {
			if ($row['type'] == 9) {
				$rank = '<span class="badge badge-pill badge-danger">Đại Ka</span>';
			} else{
				$rank = '<span class="badge badge-pill badge-secondary">Thần dân</span>';
			}


			echo '<tr>
			<th scope="row">'.$row['id'].'</th>
			<td>'.$row['username'].'</td>
			<td>'.$row['email'].'</td>
			<td>'.$rank.'</td>
			<td><a href="#!" class="btn btn-success mb-1"> <i class="fas fa-edit"></i> </a> <a href="remove.php?user='.$row['id'].'" class="btn btn-danger mb-1"> <i class="fas fa-trash-alt"></i> </a></td>
			</tr>';

		}
	}
	mysqli_close($conn);
}


function countIndex($db){
require 'db.php';
$query = "SELECT `id` FROM `$db`";
$data = mysqli_query($conn, $query);
$total = mysqli_num_rows($data);
return $total;
mysqli_close($conn);
}


function update_user($id, $username, $password, $email, $type){
	$query = "UPDATE `user` SET username='$username', password = '$password', email = '$email', type = '$type' WHERE id=$id";
	$data = mysqli_query($query);
}


function getAllCategory($page){
	require 'db.php';
	$limit = 5;
	$query = "SELECT `id` FROM `category`";
	$row = mysqli_query($conn, $query);
	$total_page = mysqli_num_rows($row);
	$current_page = ceil($total_page / $limit);
	if ($page > $current_page) {
		$page = $current_page;
	}
	$from = ($page - 1 ) * $limit;
	$query = "SELECT * FROM category LIMIT $from, $limit";
	$data = mysqli_query($conn, $query);
	if (mysqli_num_rows($data) > 0) {
		while ($row = mysqli_fetch_assoc($data)) {
			
			echo '<tr>
			<th scope="row">'.$row['id'].'</th>
			<td>'.$row['category_title'].'</td>
			<td>'.$row['description'].'</td>
			<td><a href="#!" class="btn btn-success mb-1"> <i class="fas fa-edit"></i> </a> <a href="remove.php?cat='.$row['id'].'" class="btn btn-danger mb-1"> <i class="fas fa-trash-alt"></i> </a></td>
			</tr>';

		}
	}else{
		echo 'Chưa có chuyên mục';
	}
	mysqli_close($conn);

}

function getAllPost($page){
	require 'db.php';
	$limit = 5;
	$query = "SELECT `id` FROM `post`";
	$row = mysqli_query($conn, $query);
	$total_page = mysqli_num_rows($row);
	$current_page = ceil($total_page / $limit);
	if ($page > $current_page) {
		$page = $current_page;
	}
	$from = ($page - 1 ) * $limit;
	$query = "SELECT * FROM post LIMIT $from, $limit";
	$data = mysqli_query($conn, $query);
	if (mysqli_num_rows($data) > 0) {
		while ($row = mysqli_fetch_assoc($data)) {
			
			echo '<tr>
			<th scope="row">'.$row['id'].'</th>
			<td>'.$row['title'].'</td>
			<td>'.$row['datetime'].'</td>
			<td><img width="80px" height="120px" src="'.$row['thumbnail'].'"/></td>
			<td><a href="#!" class="btn btn-success mb-1"> <i class="fas fa-edit"></i> </a> <a href="remove.php?post='.$row['id'].'" class="btn btn-danger mb-1"> <i class="fas fa-trash-alt"></i> </a></td>
			</tr>';

		}
	}else{
		echo 'Chưa có truyện nào';
	}
	mysqli_close($conn);

}

function getAllChapter($page){
	require 'db.php';
	$limit = 5;
	$query = "SELECT `id` FROM `chapter`";
	$row = mysqli_query($conn, $query);
	$total_page = mysqli_num_rows($row);
	$current_page = ceil($total_page / $limit);
	if ($page > $current_page) {
		$page = $current_page;
	}
	$from = ($page - 1 ) * $limit;
	$query = "SELECT * FROM chapter LIMIT $from, $limit";
	$data = mysqli_query($conn, $query);
	if (mysqli_num_rows($data) > 0) {
		while ($row = mysqli_fetch_assoc($data)) {
			
			echo '<tr>
			<th scope="row">'.$row['id'].'</th>
			<td>'.$row['chapter_title'].'</td>
			<td>'.$row['chapter_datetime'].'</td>
			<td>'.$row['post'].'</td>
			<td><a href="#!" class="btn btn-success mb-1"> <i class="fas fa-edit"></i> </a> <a href="remove.php?chap='.$row['id'].'" class="btn btn-danger mb-1"> <i class="fas fa-trash-alt"></i> </a></td>
			</tr>';

		}
	}else{
		echo 'Chưa có chương nào';
	}
	mysqli_close($conn);

}

function getCategoryToPost(){
	require 'db.php';
	$query = "SELECT `id`, `category_title` FROM `category`";
	$data = mysqli_query($conn, $query);
	if (mysqli_num_rows($data) > 0) {
		while ($row = mysqli_fetch_assoc($data)) {
			echo '<option value="'.$row['id'].'">'.$row['category_title'].'</option>';
		}
	}else{
		echo 'Chưa có chuyên mục';
	}
	mysqli_close($conn);

}



function insertCategory($title, $slug, $description){
	require 'db.php';
	$query = "INSERT INTO `category` (`category_title`, `slug_cat`, `description`) VALUES ('$title', '$slug', '$description')";
	$data = mysqli_query($conn, $query);
	if ($data) {
		echo '<style>#baka-success{display: block;}</style>';

	}else {
		echo '<style>#baka-failed{display: block;}</style>';
	}
	header("location: category.php");
	mysqli_close($conn);
	
}


function insertPost($title, $slug, $thumbnail, $content, $category, $author){
	require 'db.php';
	$query = "INSERT INTO `post` (`title`, `slug`, `thumbnail`, `content`, `category`, `author`) VALUES ('$title', '$slug', '$thumbnail', '$content', 
	'$category', '$author')";
	$data = mysqli_query($conn, $query);
	if ($data) {
		echo '<style>#baka-success{display: block;}</style>';

	}else {
		echo '<style>#baka-failed{display: block;}</style>';
	}
	header("location: post.php");
	mysqli_close($conn);
	
}


function insertChapter($title, $slug, $content, $post){
	require 'db.php';
	$query = "INSERT INTO `chapter` (`title`, `slug`, `content`, `post`) VALUES ('$title', '$slug', '$content', 
	'$post')";
	$data = mysqli_query($conn, $query);
	if ($data) {
		echo '<style>#baka-success{display: block;}</style>';

	}else {
		echo '<style>#baka-failed{display: block;}</style>';
	}
	header("location: chapter.php");
	mysqli_close($conn);
	
}


function baka_pagination($page, $table){
	require 'db.php';
	$limit = 5;
	$query = "SELECT `id` FROM `$table`";
	$row = mysqli_query($conn, $query);
	$total_page = mysqli_num_rows($row);
	$current_page = ceil($total_page / $limit);

	for ($i = 1; $i <= $current_page ; $i++) {

		echo '<a href="?page='.$i.'"><button type="button" class="btn btn-secondary">'.$i.'</button></a>';
		
	}
	mysqli_close($conn);

}

//Hàm chuyển tên sang url đẹp
function rwurl($title){
$replacement = '-';
$map = array();
$quotedReplacement = preg_quote($replacement, '/');
$default = array(
'/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|å/' => 'a',
'/e|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|ë/' => 'e',
'/ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|î/' => 'i',
'/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|ø/' => 'o',
'/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|ů|û/' => 'u',
'/ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ/'	=> 'y',
'/đ|Đ/' => 'd',
'/ç/' => 'c',
'/ñ/' => 'n',
'/ä|æ/' => 'ae',
'/ö/' => 'o',
'/ü/' => 'u',
'/Ä/' => 'A',
'/Ü/' => 'U',
'/Ö/' => 'O',
'/ß/' => 'b',
'/̃|̉|̣|̀|́/' => '',
'/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ', '/\\s+/' => $replacement,
sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',
);
$title = urldecode($title);
mb_internal_encoding('UTF-8');
$map = array_merge($map, $default);
return strtolower( preg_replace(array_keys($map), array_values($map), $title) );
}




?>

